# flutter_httputil

#### 介绍
用dio简单封装get和post请求

#### 安装教程

1. 使用dio先在pubspec.yaml添加dio包，然后packages get，获取最新的版本https://pub.dev/packages/dio
~~~
dio: ^2.1.7
~~~

2. 引用HttpUtil.dart
~~~
import 'package:test_app/HttpUtil.dart';
~~~

#### 使用说明

1. 使用get请求
~~~
HttpUtil.get(
  url,
  data: {
    key: value
  },
  headers: {
　　key: value
　}
  success: (data){
    // 请求成功返回的数据
  },error: (errorMsg){
    // 请求失败返回的错误信息
  }
);

// 例如
HttpUtil.get(
  '/vod/getPlayInfo',
  data: {
    'userId': 'DWF132512F1SV1D5F2VD1F5'
  },
  headers: {
    'token': 'ASD1A210F2S1FD5S1251F.1DFS3V51S5VFS'
  },
  success: (data){
  	// 请求成功返回的数据
    print(data.toString());
  },error: (errorMsg){
  	// 请求失败返回的错误信息
    print(errorMsg);
  }
);
~~~

2. 使用post请求
~~~
HttpUtil.post(
  url,
  data: {
    key: value
  },
  headers: {
　　key: value
　}
  success: (data){
    // 请求成功返回的数据
  },error: (errorMsg){
    // 请求失败返回的错误信息
  }
);
~~~

#### 注意
1. 该方法处理后端返回的格式为
~~~
{
  "code": int,
  "msg": "String",
  "data": dynamic
}
~~~
