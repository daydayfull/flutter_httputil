import 'package:flutter/material.dart';
import 'package:test_app/HttpUtil.dart';

class HttputilTest extends StatefulWidget {
  @override
  _HttputilTestState createState() => _HttputilTestState();
}

class _HttputilTestState extends State<HttputilTest> {
  String _data = '正在请求数据……';

  @override
  void initState() {
    // TODO: implement initState
    _sendRequest();
    super.initState();
  }

  void _sendRequest(){
    // 发起请求
    HttpUtil.get(
      '/app_model/httptest.json',
      success: (data){
        setState(() {
          _data = '数据返回成功：'+data.toString();
        });
      },error: (errorMsg){
      setState(() {
        _data = '数据返回失败：'+errorMsg;
      });
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('测试http请求',style: TextStyle(color: Colors.white,fontSize: 20),),
      ),
      body: Center(
        child: Text(_data),
      ),
    );
  }

}
